# **_satellite simulation_**


## Description

This project is a physics-based simulation that simulates the elliptical motion of the satellite and its effect on the different planets it serves, its movement between several orbits, and its impact on the gravitational pull of several planets on the Satellite.
Giving accurate details of the location and speed of the Satellite in Real Time.
___
## Badges

- ![Vite](https://img.shields.io/badge/Vite-v2.9.9-lightgrey)

* ![THREE.js](https://img.shields.io/badge/Three.js-r140-green)
___
## Visuals

![GUI](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/raw/master/Photos/GUI.png)

---

- Gravity*constant: **_Main Planet Gravity Constant_** you can change it by entering a value or drag the bar.

* airDensity: _**Air Density in the Space**_ you can change it by entering a value or drag the bar.
* planetss: _**The 'Main' planet which satellite serve**_ you can change it to {Earth ,Mars ,Venus}.
  -  ## Earth
  >>![Earth](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/raw/master/Photos/Earth%20text.png)
  - ## Mars
  >>![Mars](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/raw/master/Photos/MarsText.png)
  - ## venus
  >>![venus](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/raw/master/Photos/venusText.png)

- ### Moon: _**To see the gravitational pull of several planets / Stars (in our condition its a Star)**_ its a check box when you checked it the Moon Start Effect on the satellite.
* ### Moon_Mass: **_The mass of the Moon_** you can change it by entering a value or drag the bar.
* ### Planet_gravity_const: _**The Moon gravity Constant**_ you can change it by entering a value or drag the bar.
* ### Visible: **_to draw the Path of  Satellite Movement_** its a check box when you checked it the Drawing, and the color of the path change based on the Planet{Earth ,Mars ,Venus}.
   -  ## Earth
  >>![Earth](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/raw/master/Photos/earth.png)
  - ## Mars
  >>![Mars](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/raw/master/Photos/mars.png)
  - ## venus
  >>![venus](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/raw/master/Photos/venus.png)
* ### Transfer: **_to change the Satellite Orbit and Distance between him and the "Main Planet"_** on press you will give him a scale of 5 as a boost
   - ![Transfer](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/raw/master/Photos/transfer.png)

* ### TimeDiff: **_To make the Simulation Slower/Faster_** by changing the value of it to smaller or higher

## Installation

- First you must have Node.js in your divece
  here is a [Link](https://nodejs.org/en/download/)

* run npm commands on your terminal:

```
$ npm install
$ npm run dev
```

- and the simulation should be running on localHost and you can open it in any browser Best performance:(Microsoft edge , Google chrome)
___

## Run Example:
[![video session](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/raw/master/Photos/ex.png)](https://gitlab.com/the-teachers-public/satellite-simulation-the-teachers/-/blob/master/Photos/Satellite%20Simulation.mp4)
___
## Support

**_For Support you can Contact me at:_** Majd_ma@outlook.com
___

## Refrensecs
* [Keeping satellites in orbit] [University of Waikato] [URL](https://www.sciencelearn.org.nz/videos/127-keeping-satellites-in-orbit)
* [Drag Coefficient] [Science Direct] [URL](https://www.sciencedirect.com/topics/engineering/drag-coefficient)
* [What Is a Satellite?] [URL](https://www.nasa.gov/audience/forstudents/5-8/features/nasa-knows/what-is-a-satellite-58.html)
* [Drag Coefficient] E.L. Houghton, ... Daniel T. Valentine, in Aerodynamics for Engineering Students (Sixth Edition), 2013
* [Drag Coefficient] G.D. Catalano Assistant Professor, ... J.D. Lambert, in Energy Developments: New Forms, Renewables, Conservation, 1984
* [Atmospheric drag] Kyle T. Alfriend, ... Louis S. Breger, in Spacecraft Formation Flying, 2010

