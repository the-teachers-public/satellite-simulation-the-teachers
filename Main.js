// THREE JS
import './style.css'
import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import * as dat from 'dat.gui'
import { radToDeg } from 'three/src/math/mathutils';
import { Raycaster } from 'three';

const progressBar = document.getElementById("progress-bar");
const progressBarContainer = document.querySelector('.progress-bar-container')

//loading manager
const loadingManager = new THREE.LoadingManager();

loadingManager.onProgress = function(url, loaded, total) {
    progressBar.value = (loaded / total) * 100;
}

loadingManager.onLoad = function() {
    progressBarContainer.style.display = 'none';
}



// TextureLoader
const textureLoader = new THREE.TextureLoader(loadingManager)
    // Textures of particals

const partical_textrure = textureLoader.load('/textures/particles/9.png')

//planet texture
const earth = new THREE.TextureLoader().load('/textures/planets/earth.jpg')
const mars = new THREE.TextureLoader().load('/textures/planets/mars.png')
const jupiter = new THREE.TextureLoader().load('/textures/planets/jupiter.jpg')
const venus = new THREE.TextureLoader().load('/textures/planets/venus.jpg')
const extraplanet = new THREE.TextureLoader().load('/textures/planets/extraplanet.jpg')




//canvas
const canvas = document.querySelector('canvas.webgl')

const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

// Scene
const scene = new THREE.Scene();

//Light
var light = new THREE.AmbientLight(0xFFFFFF, 1);
scene.add(light);


// Camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 80000);
camera.position.z = 4100;


// Controls
const controls = new OrbitControls(camera, canvas)
controls.target.set(0, 0.75, 0)
controls.enableDamping = true
controls.autoRotate = false;


// renderer
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
});


// GUI
window.onload = () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
}

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

//masses of plants
var masses = {
    Earth: 5.97219 * 10000000000000,
    mars: 5 * 10000000000000,
    venus: 4.8 * 10000000000000
}


var planetss = {
    Earth: 'earth',
    Mars: 'mars',
    Venus: 'venus'
}

// degug GUI
const gui = new dat.GUI({ closed: false, width: 300, color: 'white' });
//gui.hide(); // enter H to show controls
const EarthFolder = gui.addFolder('Earth');
EarthFolder.open();
const PlanetFolder = gui.addFolder('Moon');
PlanetFolder.open();
const SatelliteFolder = gui.addFolder('Satellite');
SatelliteFolder.open();
const parameters = {

    Gravity_constant: (6.6743),
    mass_earth: (5.97219 * 10000000000000),
    airDensity: ((0.0000000001322) / (2.0769 * 2.73)) * 1000000,
    planetss: planetss.Earth,


    //Planet
    Planet: false,
    Planet_gravity_const: (6.6743),
    Planet_mass: (7.34767309 * 100000000000),
    // Satellite
    massofsatellite: 2500,
    visible: false,
    transfer: 1,
    timeDiff: 0.2
}


EarthFolder.add(parameters, 'Gravity_constant')
    .onChange(() => {
        Gravity_constant = parameters.Gravity_constant
    })

EarthFolder.add(parameters, 'airDensity').max(5.5)
    .onFinishChange(() => {
        airDensity = parameters.airDensity
    })

EarthFolder.add(parameters, 'planetss').options(planetss).onChange(() => {

    if (parameters.planetss == 'mars') {
        material2.map = mars
        parameters.mass_earth = masses.mars
        EarthMassConst = parameters.mass_earth
    } else if (parameters.planetss == 'earth') {
        material2.map = earth
        parameters.mass_earth = masses.Earth
        EarthMassConst = parameters.mass_earth
    } else if (parameters.planetss == 'venus') {
        material2.map = venus
        parameters.mass_earth = masses.venus
        EarthMassConst = parameters.mass_earth
    }

})


PlanetFolder.add(parameters, 'Planet')
    .onChange(() => {
        Planet = parameters.Planet
    })
PlanetFolder.add(parameters, 'Planet_mass')
    .onChange(() => {
        Planet_mass = parameters.Planet_mass
    })
PlanetFolder.add(parameters, 'Planet_gravity_const')
    .onChange(() => {
        Planet_gravity_const = parameters.Planet_gravity_const
    })




SatelliteFolder.add(parameters, 'transfer').min(1).max(5).step(0.5)
    .onFinishChange(() => {
        transfer = parameters.transfer;
        curo = curVelocity.clone();
        curo.multiplyScalar(speed * transfer);
        curVelocity.add(curo);
    })
SatelliteFolder.add(parameters, 'visible')
    .onChange(() => {
        visible = parameters.visible
    })

SatelliteFolder.add(parameters, 'timeDiff')
    .onChange(() => {
        timeDiff = parameters.timeDiff
    })

//particals
const particalsGeometry = new THREE.BufferGeometry(1, 32, 32)

// stars
const count = 5000 //to increase stars
const positions = new Float32Array(count * 3)

// const colors = new Float32Array(count*3)
for (let i = 0; i < count; i++) {
    positions[i] = (Math.random() - 0.5) * 20000 //to increase distance between stars

}

particalsGeometry.setAttribute(
    'position',
    new THREE.BufferAttribute(positions, 3)
)


const particalsMaterial = new THREE.PointsMaterial({
    size: 0.1,
    sizeAttenuation: true,
    transparent: true,
    color: 'white',
    map: partical_textrure
})
particalsMaterial.alphaTest = 0.001
particalsMaterial.depthWrite = false
particalsMaterial.blending = THREE.AdditiveBlending


//points
const particals = new THREE.Points(particalsGeometry, particalsMaterial)
    // particals.scale.set()
scene.add(particals)

//Model loader
const loader = new GLTFLoader(loadingManager);

var SateModel;
loader.load('/models/s6laaaaay.glb', function(glb) {
        SateModel = glb.scene;
        SateModel.scale.set(30, 30, 30)
        scene.add(SateModel);
        SateModel.position.set(0, 0, 0)
        SateModel.rotateX(45)
    },
    undefined,
    function(error) {
        console.error(error);
    });


// Satellite
const geometry = new THREE.SphereGeometry(15, 32, 16);
const material = new THREE.MeshBasicMaterial({ color: 0xFF0000 });
var satellite = new THREE.Mesh(geometry, material);
satellite.scale.set(10, 10, 10);
satellite.position.set(0, 0, -3000);

// Earth
const geometry2 = new THREE.SphereGeometry(30, 32, 16);
const material2 = new THREE.MeshBasicMaterial({ map: earth });
var Earth = new THREE.Mesh(geometry2, material2);
Earth.scale.set(10, 10, 10)
Earth.position.set(0, 0, 0)
scene.add(Earth)

// Planet
const geometry3 = new THREE.SphereGeometry(50, 32, 16);
const Plaent_material = new THREE.MeshBasicMaterial({ map: extraplanet });
//const Plaent_material = new THREE.MeshBasicMaterial({ color: 0xFFFFFF });
var Ran_Planet = new THREE.Mesh(geometry3, Plaent_material);
Ran_Planet.scale.set(5, 5, 5)
Ran_Planet.position.set(-2000, 6000, 0)
scene.add(Ran_Planet)



var vec = new THREE.Vector3(); // create once and reuse
var pos = new THREE.Vector3(); // create once and reuse



function onPointerMove(event) {

    vec.set(
        (event.clientX / window.innerWidth) * 2 - 1, -1 * (event.clientY / window.innerHeight) * 2 + 1,
        0.5);
    vec.unproject(camera);
    vec.sub(camera.position).normalize();
    var distance = Earth.position.z - camera.position.z / vec.z;

    pos = pos.copy(camera.position).add(vec.multiplyScalar(distance));



}




window.addEventListener('pointermove', onPointerMove);
window.addEventListener('dblclick', onPointerClick);

// Delta time
var timeDiff = parameters.timeDiff;

// Consts
// air density = p / R*T:p for absolute pressure, R for specific gas constant,T for absolute temperature
// R_helium= 2.0769, R_hydrogen= 4.124
var stop = true
var HideInfo = false;
const speed = 0.04;
const dragCoefficient = 2.2;
const satelitteArea = 0.0255744
var Gravity_constant = parameters.Gravity_constant
var EarthMassConst = parameters.mass_earth
var airDensity = parameters.airDensity
var massofsatellite = parameters.massofsatellite;
var gravity = new THREE.Vector3();
// Sec Planet consts
var Planet_gravity_const = parameters.Planet_gravity_const
var Planet_mass = parameters.Planet_mass
var gravity2 = new THREE.Vector3();
var visible = false
var Planet = parameters.Planet
var transfer = 1

// starting Velocity
var curVelocity = new THREE.Vector3(300, 0, 0);
var curo = curVelocity.clone()

var points = [];

function onPointerClick() {
    if (Math.abs(camera.position.z) < Math.abs(camera.position.y) || Math.abs(camera.position.z) < Math.abs(camera.position.x)) {
        camera.position.set(0, 0, 4100);
    } else {
        curVelocity = new THREE.Vector3(300, 0, 0);
        if (!isNaN(pos.x))
            satellite.position.x = pos.x;
        if (!isNaN(pos.y))
            satellite.position.y = pos.y;
        if (!isNaN(pos.z))
            satellite.position.z = pos.z;
    }
}

// Drag force : not stable
function DragForce(velocity) {
    var force = (0.5 * airDensity * dragCoefficient * satelitteArea * velocity * velocity) / ((Earth.position.clone().sub(satellite.position).length()) / 100)
    if (isNaN(force))
        force = 0
        //console.log(force)
    if (force != 0)
        force = force * -1 * velocity / Math.abs(velocity);

    return force;
}


// GravityForce: The farther the satellite on Earth the less Fg value
function GravityForce() {
    var force = (EarthMassConst * Gravity_constant * massofsatellite) / (Earth.position.clone().sub(satellite.position).lengthSq())
    force = force / 1000000;
    if (isNaN(force))
        force = 0
        // console.log(EarthMassConst)
    return force
}

function GravityForce2() {
    var force = (Planet_mass * Planet_gravity_const * massofsatellite) / (Ran_Planet.position.clone().sub(satellite.position).lengthSq())
    force = force / 1000000;
    if (isNaN(force))
        force = 0
        //console.log(force)
    return force
}

// Calculate the new acceleration based on equation F=ma
function calculate() {
    if (Planet)
        var Fg2 = GravityForce2()
    else
        var Fg2 = 0

    var Fg = GravityForce()

    // the vactor of the garvity * Fg
    gravity = Earth.position.clone().sub(satellite.position).normalize().multiplyScalar(Fg);
    gravity2 = Ran_Planet.position.clone().sub(satellite.position).normalize().multiplyScalar(Fg2);
    //console.log(gravity2)
    if (isNaN(gravity.x))
        gravity.x = 0

    if (isNaN(gravity.y))
        gravity.y = 0

    if (isNaN(gravity.z))
        gravity.z = 0

    if (isNaN(gravity2.x))
        gravity2.x = 0

    if (isNaN(gravity2.y))
        gravity2.y = 0

    if (isNaN(gravity2.z))
        gravity2.z = 0

    var accX = (gravity.x + gravity2.x + DragForce(curVelocity.x)) / massofsatellite;

    var accY = (gravity.y + gravity2.y + DragForce(curVelocity.y)) / massofsatellite;

    var accZ = (gravity.z + gravity2.z + DragForce(curVelocity.z)) / massofsatellite;

    // checking on the values of the new acceleration
    if (isNaN(accX))
        accX = 0
    if (isNaN(accY))
        accY = 0
    if (isNaN(accZ))
        accZ = 0

    var acceleration = new THREE.Vector3(accX, accY, accZ)

    newposition(acceleration)
}


// function for calculating the new position based on the current acceleration
function newposition(acceleration) {


    points.push(new THREE.Vector3(satellite.position.x, satellite.position.y, satellite.position.z));

    curVelocity.x = curVelocity.x + acceleration.x * timeDiff;
    curVelocity.y = curVelocity.y + acceleration.y * timeDiff;
    curVelocity.z = curVelocity.z + acceleration.z * timeDiff;

    satellite.position.x = satellite.position.x + curVelocity.x * timeDiff;
    satellite.position.y = satellite.position.y + curVelocity.y * timeDiff;
    satellite.position.z = satellite.position.z + curVelocity.z * timeDiff;



    SateModel.position.set(satellite.position.x, satellite.position.y, satellite.position.z)




    if (visible) {

        var material;
        points.push(new THREE.Vector3(satellite.position.x, satellite.position.y, satellite.position.z));
        if (parameters.planetss == 'earth')
            material = new THREE.PointsMaterial({ color: "#4E50DE" });
        if (parameters.planetss == 'mars')
            material = new THREE.PointsMaterial({ color: "#EA2326" });
        if (parameters.planetss == 'venus')
            material = new THREE.PointsMaterial({ color: "#E4DC6C" });
        const geometry = new THREE.BufferGeometry().setFromPoints(points);
        const line = new THREE.Line(geometry, material);

        scene.add(line);
        points.pop();
    }
    points.pop();
}


// Pause Running : when you press 'S' the simulating pause and when it pressed again it will start again from the stoping point
document.addEventListener("keydown", onDocumentKeyDown, false);


function onDocumentKeyDown(event) {
    var keyCode = event.which;
    if (keyCode == 83) {
        if (stop == false)
            stop = true;
        else
            stop = false;

    }
    if (keyCode == 70) {
        if (HideInfo == false)
            HideInfo = true;
        else
            HideInfo = false;

    }
};




// Display The Values on the Screen
const text2 = document.createElement('div');
text2.id = "info"
text2.style.position = 'absolute';
text2.style.width = "100";
text2.style.height = "100";
//text2.style.backgroundColor = "white";
text2.style.top = "18" + 'px';
text2.style.left = "10" + 'px';
text2.hidden = true;
document.body.appendChild(text2);
var generateTextOnScreen = () => {
    var text = ' Planet Name: ' + parameters.planetss;
    text += '<br>';
    text += ' Planet Mass : ' + parameters.mass_earth;
    text += '<br>';
    text += ' Gravity_constant: ' + parameters.Gravity_constant;
    text += '<br>';
    text += ' Satellite position in x: ' + satellite.position.x;
    text += '<br>';
    text += ' Satellite position in y: ' + satellite.position.y;
    text += '<br>';
    text += ' Satellite position in z: ' + satellite.position.z;
    text += '<br>';
    text += ' Satellite speed in x: ' + curVelocity.x;
    text += '<br>';
    text += ' Satellite speed in y: ' + curVelocity.y;
    text += '<br>';
    text += ' Satellite speed in z: ' + curVelocity.z;
    text += '<br>';
    text += ' Thrust: ' + parameters.transfer;
    text += '<br>';
    text += ' TimeDiff: ' + parameters.timeDiff;
    text += '<br>';
    text2.innerHTML = text;
}


// animate
function animate() {
    setTimeout(animate, 20);
    if (SateModel) {
        if (Earth.position.clone().sub(satellite.position).length() < 70000 && Earth.position.clone().sub(satellite.position).length() > 600) {
            if (stop == false)
                calculate();
        }

        if (HideInfo) {
            text2.hidden = false;
        } else {
            text2.hidden = true;
        }
        generateTextOnScreen();
        // Update controls
        controls.update()
        Earth.rotateY(-0.002);
        Ran_Planet.rotateY(-0.002);
        renderer.render(scene, camera);

    }

}

animate();